import { Formik, ErrorMessage, Field, Form } from "formik";
import { Link, useHistory } from "react-router-dom";
import "../App.css";
import * as Yup from "yup";
import { signUp } from "../slices/userSlice";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-bootstrap";

const SignUp = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user || {});
  const { errorMessage } = user;
  
  const signUpSchema = Yup.object().shape({
    fullname: Yup.string()
      .required("Fullname is required")
      .min(1, "Fullname is too short"),

    email: Yup.string().email("invalid email address").required("Required"),

    password: Yup.string()
      .min(3, "Password is too short - should be 3 chars minimum")
      .required("Password is required")
      .matches(/(?=.*[0-9])/, "Password must contain a number"),
    // .matches(
    //   /(?=.*[●!"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~])/,
    //   "Password must contain a symbol"
    // ),

    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });

  const initialValues = {
    fullname: "",
    email: "",
    password: "",
    password_confirmation: "",
  };

  const handleRegister = (value) => {
    dispatch(signUp(value))
      .then(() => {
        toast.success("Registered Successfully", {
          draggable: false,
          position: toast.POSITION.BOTTOM_LEFT,
          icon: "😊",
        });
        history.push("/");
      })
      .catch(() => {
        toast.error({
          draggable: false,
          message : errorMessage,
          position: toast.POSITION.BOTTOM_LEFT,
          icon: "",
        });
        
      });
  };

  // const handleRegister = async (values) => {
  //   const requestBody = {
  //     fullname: values.fullname,
  //     email: values.email,
  //     password: values.password,
  //     password_confirmation: values.password_confirmation,
  //   };
  //   console.log(requestBody);
  //   try {
  //     const response = await fetch("https://accounts.tujjudemo.com/api/user", {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify(requestBody),
  //     });
  //     let data = await response.json();
  //     console.log("data", data);

  //     if (response.status === 200 || response.status === 201) {
  //       history.push("/");
  //     }
  //   } catch (e) {
  //     console.log("Error", e.response.data.error.email);
  //     setErrorMessage(e.message)
  //   }
  // };

  return (
    <div className="login">
      <div className="sign-container">
        <div className="sign-wrapper" id="sign-wrapper">
          <ToastContainer autoClose={1000} />
        </div>
      </div>
      <Formik
        initialValues={initialValues}
        validationSchema={signUpSchema}
        onSubmit={handleRegister}
      >
        {(formik) => {
          const { errors, touched } = formik;
          return (
            <Form className="login__form">
              <h1>Signup Here 🚀</h1>
              <div className="form">
                <Field
                  autoComplete="off"
                  type="fullname"
                  name="fullname"
                  placeholder="Full Name"
                  className={errors.fullname && touched.fullname && "error"}
                />
                <ErrorMessage
                  name="fullname"
                  component="span"
                  className="error"
                />
              </div>
              <div className="form">
                <Field
                  autoComplete="off"
                  type="email"
                  name="email"
                  placeholder="Email"
                  className={errors.email && touched.email && "error"}
                />
                <ErrorMessage name="email" component="span" className="error" />
              </div>
              <div className="form">
                <Field
                  autoComplete="off"
                  type="password"
                  name="password"
                  placeholder="Password"
                  className={errors.password && touched.password && "error"}
                />
                <ErrorMessage
                  name="password"
                  component="span"
                  className="error"
                />
              </div>
              <div className="form">
                <Field
                  autoComplete="off"
                  type="password"
                  name="password_confirmation"
                  placeholder="Password Confirmation"
                  className={
                    errors.password_confirmation &&
                    touched.password_confirmation &&
                    "error"
                  }
                />
                <ErrorMessage
                  name="password_confirmation"
                  component="span"
                  className="error"
                />
              </div>

              <button type="submit" className="submit__btn">
                Signup
              </button>
              <span>
                Or <Link to="login"> Login</Link>
              </span>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default SignUp;
