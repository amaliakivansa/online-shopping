import { useState, useCallback } from "react";
import { Form, FormControl } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { addToCart } from "../slices/cartSlice";
import _ from "lodash";
import Pagination from "./Pagination";

const Home = () => {
  const products = useSelector((state) => state.products);
  const dispatch = useDispatch();
  const history = useHistory();
  const [toggle, setToggle] = useState(false);
  const [setSearchInput] = useState("");
  const [filteredResults, setFilteredResults] = useState("");
  const [debouncedValue, setDebouncedValue] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage] = useState(6);

  // Search
  const handleChange = (e) => {
    setSearchInput(e.target.value);
    debounce(e.target.value);
    if (debouncedValue !== "") {
      const filteredProduct = products.filter((product) => {
        return Object.values(product)
          .join("")
          .toLowerCase()
          .includes(debouncedValue.toLowerCase());
      });
      console.log(filteredProduct);
      setFilteredResults(filteredProduct);
    } else {
      setFilteredResults(products);
    }
  };

  // Debounce
  const debounce = useCallback(
    _.debounce((_searchVal) => {
      setDebouncedValue(_searchVal);
    }, 500),
    []
  );

  // Handle Add to Cart
  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
    history.push("/cart");
  };

  // Get Current Posts
  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProducts = products.slice(
    indexOfFirstProduct,
    indexOfLastProduct
  );

  // Change Page
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className="home-container">
      {
        <>
          <div className="search">
            <div
              className={`mt-2 ${!toggle ? "search-form" : "search-form-2"}`}
            >
              {/* form search */}
              <Form className="d-flex">
                <FormControl
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                  value={debouncedValue}
                  onChange={handleChange}
                />
                {/* <Button variant="outline-success">Search</Button> */}
              </Form>
              {/* icon remove */}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="40"
                fill="black"
                className="bi bi-x me-4"
                viewBox="0 0 16 16"
                onClick={() => setToggle()}
              >
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </div>
            {/* icon search */}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="27"
              height="27"
              fill="black"
              className={`bi bi-search mt-2 me-2 ${
                toggle ? "remove-icon" : "display-icon"
              }`}
              viewBox="0 0 16 16"
              onClick={() => setToggle(!toggle)}
            >
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
            </svg>
          </div>
          <h2 className="mt-5">New Arrivals</h2>
          <div className="products">
            {debouncedValue.length > 1
              ? filteredResults.map((product) => (
                  <div key={product.id} className="product">
                    <h3>
                      {product.name.length > 22
                        ? product.name.substr(0, 22) + "..."
                        : product.name}
                    </h3>
                    <img src={product.image} alt={product.name} />
                    <div className="details">
                      <span>{product.desc}</span>
                      <span className="price">${product.price}</span>
                    </div>
                    <button onClick={() => handleAddToCart(product)}>
                      Add to cart
                    </button>
                  </div>
                ))
              : currentProducts.map((product) => (
                  <div key={product.id} className="product">
                    <h3>
                      {product.name.length > 22
                        ? product.name.substr(0, 22) + "..."
                        : product.name}
                    </h3>
                    <img src={product.image} alt={product.name} />
                    <div className="details">
                      <span>{product.desc}</span>
                      <span className="price">${product.price}</span>
                    </div>
                    <button onClick={() => handleAddToCart(product)}>
                      Add to cart
                    </button>
                  </div>
                ))}
          </div>
        </>
      }
      <Pagination
        productsPerPage={productsPerPage}
        totalProducts={products.length}
        paginate={paginate}
      />
    </div>
  );
};

export default Home;
