import "../App.css";
import { useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { useState } from "react";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { login } from "../slices/userSlice";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-bootstrap";

toast.configure()

const Login = () => {
  const [loginError] = useState();
 

  const history = useHistory();
  const dispatch = useDispatch();

  const signInSchema = Yup.object().shape({
    email: Yup.string().email("invalid email address").required("Required"),

    password: Yup.string()
      .min(3, "Password is too short - should be 3 chars minimum")
      .required("Password is required"),
  });

  const handleLogin = (values) => {
    dispatch(login(values)).then(() => {
      toast.success("login successfully", {
        draggable: false,
        position: toast.POSITION.BOTTOM_LEFT,
        icon: "😊"
      });
      history.push('/');
    }).catch(() => {
      toast.error("The username or password is incorrect", {
        draggable: false,
        position: toast.POSITION.BOTTOM_LEFT,
        icon: "☹️"
      });
    });
  };

  // const handleLogin = async (values) => {
  //   const bodyFormData = new FormData();
  //   bodyFormData.append("grant_type", "password");
  //   bodyFormData.append("client_id", "7b48f795-a4b1-4506-8f48-d955935e4002");
  //   bodyFormData.append("client_secret", "demo-secret");
  //   bodyFormData.append("username", values.email);
  //   bodyFormData.append("password", values.password);
  //   bodyFormData.append("scope", "*");

  //   try {
  //     const response = await axiosInstance({
  //       method: "post",
  //       url: "/api/oauth/token",
  //       data: bodyFormData,
  //       headers: {
  //         "Content-Type": "multipart/form-data",
  //       },
  //     });
  //     console.log(response.data.data);
  //     let currTime = new Date();
  //     let timeExp = response.data.data.expires_in;
  //     let tokenExpired = currTime.getTime() / 1000 + timeExp;

  //     if (response.status === 200) {
  //       localStorage.setItem("token", response.data.data.access_token);
  //       localStorage.setItem("token-expired", tokenExpired);
  //       history.push("/");
  //     } else {
  //       console.log("Error", response.error);
  //     }
  //   } catch (e) {
  //     console.log("Error", e.response.data.data);
     
  //   }
  // };

  const initialValues = {
    email: "",
    password: "",
  };

  return (
    <div className="login">
      <div className="sign-container">
        <div className="sign-wrapper" id="sign-wrapper">
          <ToastContainer autoClose={1000} /> 
        </div>
      </div>
      <Formik
        initialValues={initialValues}
        validationSchema={signInSchema}
        onSubmit={handleLogin}
      >
        {(formik) => {
          const { errors, touched } = formik;
          return (
            <Form className="login__form">
              <h1>Login Here 🚀</h1>
              <div className="form">
                <Field
                  autoComplete="off"
                  type="email"
                  placeholder="Email"
                  name="email"
                  id="email"
                  className={errors.email && touched.email && "Error"}
                />
                <ErrorMessage name="email" component="span" className="error" />
              </div>
              <div className="form">
                <Field
                  autoComplete="off"
                  type="password"
                  placeholder="Password"
                  name="password"
                  id="password"
                  className={errors.password && touched.password && "Error"}
                />
                <ErrorMessage
                  name="password"
                  component="span"
                  className="error"
                />
              </div>
              {loginError && (
                <div style={{ color: "red" }}>
                  <span>{loginError}</span>
                </div>
              )}
              <button type="submit" className="submit__btn">
                Login
              </button>
              <span className="mt-2">
                Don't have an account? <Link to="signup">Sign Up</Link>
              </span>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default Login;
