import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import {
  Container,
  NavDropdown,
  Navbar,
  Nav,
  Button,
  Modal,
} from "react-bootstrap";
import { reset } from "../slices/userSlice";
import { useState } from "react";

const NavBar = () => {
  const { cartTotalQuantity } = useSelector((state) => state.cart);
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user || {});
  // console.log(user);

  // useEffect(() => {
  //   dispatch(getUser(local))
  // })
  const { isSuccess } = user;

  // const [toggle, setToggle] = useState(false);
  // const [searchInput, setSearchInput] = useState('');
  // const [filteredResults, setFilteredResults] = useState('');

  // const searchProducts = (searchValue) => {
  //   setSearchInput(searchValue);
  //   if(searchInput !== ''){
  //     const filteredProduct = products.filter((product) => {
  //       return Object.values(product).join('').toLowerCase().includes(searchInput.toLowerCase())
  //     })
  //     console.log(filteredProduct);
  //     setFilteredResults(filteredProduct)
  //   } else {
  //     setFilteredResults(products)
  //   }
  // }
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleLogout = () => {
    setShow(false);
    localStorage.clear();
    dispatch(reset());
    history.push("/login");
  };

  return (
    <>
      <Navbar bg="dark" variant="dark" expand="lg" className="nav-bar">
        <Container>
          <Navbar.Brand>
            <Link to="/">
              <h2>Online Shop</h2>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse className="justify-content-end flex-grow-0">
            {isSuccess && (
              <>
                <Nav className="me-auto">
                  <Nav.Link href="#link" className="text-white">
                    <Link to="/cart">
                      <div className="nav-bag">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="35"
                          height="35"
                          fill="currentColor"
                          className="bi bi-handbag-fill ml-3"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 1a2 2 0 0 0-2 2v2H5V3a3 3 0 1 1 6 0v2h-1V3a2 2 0 0 0-2-2zM5 5H3.36a1.5 1.5 0 0 0-1.483 1.277L.85 13.13A2.5 2.5 0 0 0 3.322 16h9.355a2.5 2.5 0 0 0 2.473-2.87l-1.028-6.853A1.5 1.5 0 0 0 12.64 5H11v1.5a.5.5 0 0 1-1 0V5H6v1.5a.5.5 0 0 1-1 0V5z" />
                        </svg>
                        <span className="bag-quality">
                          <span>{cartTotalQuantity}</span>
                        </span>
                      </div>
                    </Link>
                  </Nav.Link>
                  <Nav.Link href="#link" className="text-white">
                    <Link to="/agent">
                      <div className="nav-chat">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="28"
                          height="28"
                          fill="currentColor"
                          className="bi bi-chat-left-dots mt-2"
                          viewBox="0 0 16 16"
                        >
                          <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H4.414A2 2 0 0 0 3 11.586l-2 2V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12.793a.5.5 0 0 0 .854.353l2.853-2.853A1 1 0 0 1 4.414 12H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                          <path d="M5 6a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                      </div>
                    </Link>
                  </Nav.Link>
                  <NavDropdown
                    title="Hello, "
                    // title={<span className="text-white">{fullname}</span>}
                    id="basic-nav-dropdown"
                  >
                    <NavDropdown.Item
                      className="text-dark"
                      onClick={handleShow}
                    >
                      Logout
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav>
              </>
            )}
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Modal
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={show}
        onHide={handleClose}
      >
        {/* <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header> */}
        <Modal.Body className="text-center p-4">
          Are you sure you want to logout ❓❓❓
        </Modal.Body>
        <Modal.Footer>
          <Button variant="light" onClick={handleClose}>
            Close
          </Button>
          <Button variant="dark" onClick={handleLogout}>
            Logout
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default NavBar;
