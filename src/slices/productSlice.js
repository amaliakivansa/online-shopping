import { createSlice } from "@reduxjs/toolkit";

const initialState = [
        {
            "id" : 1,
            "name" : "The Things You Can See Only When You're Slow Down",
            "desc" : "Self Improvement",
            "price" : 10.99,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1638516023/online-shop/book-1_vlhhne.jpg"
    
        },
        {
            "id" : 2,
            "name" : "Rich Dad Poor Dad",
            "desc" : "Financial Awareness",
            "price" : 6.82,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1638776871/online-shop/book-2_c41exw.jpg"
    
        },
        {
            "id" : 3,
            "name" : "I'll Give You the Sun",
            "desc" : "Fiction",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1638777187/online-shop/book-3_mwswkb.jpg"
    
        },
        {
            "id" : 4,
            "name" : "How to Win Friends & Influence People",
            "desc" : "Self Improvement",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644812875/online-shop/book-3_c1a8a4.jpg"
    
        },
        {
            "id" : 5,
            "name" : "Filosofi Teras",
            "desc" : "Self Improvement",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644812880/online-shop/book-4_hbzoch.jpg"
    
        },
        {
            "id" : 6,
            "name" : "Bumi Manusia",
            "desc" : "Fiction",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644812877/online-shop/book-8_lf4d9n.jpg"
    
        },
        {
            "id" : 7,
            "name" : "Hai, Miiko! 16",
            "desc" : "Comic",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644812876/online-shop/book-5_bhlnaq.jpg"
    
        },
        {
            "id" : 8,
            "name" : "Shalat Penuh Makna",
            "desc" : "Spiritual Books",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644812876/online-shop/book-6_vnwoom.jpg"
    
        },
        {
            "id" : 9,
            "name" : "Tuesday with Morrie",
            "desc" : "Spiritual Books",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644812876/online-shop/book-7_fjbfj8.jpg"
    
        },
        {
            "id" : 10,
            "name" : "Spring in London",
            "desc" : "Fiction",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644919137/online-shop/book-10_ri2xyg.jpg"
    
        },
        {
            "id" : 11,
            "name" : "Summer in Seoul",
            "desc" : "Fiction",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644919145/online-shop/book-11_m80gbt.jpg"
    
        },
        {
            "id" : 12,
            "name" : "Pasar Modal Syariah",
            "desc" : "Financial Awareness",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644919145/online-shop/book-12_efdevs.jpg"

        },
        {
            "id" : 13,
            "name" : "Winter in Tokyo",
            "desc" : "Spiritual Books",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644919146/online-shop/book-13_eiumfl.jpg"

        },
        {
            "id" : 14,
            "name" : "Pingkan Melipat Jarak",
            "desc" : "Poetry",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644919145/online-shop/book-14_slngsj.jpg"

        },
        {
            "id" : 15,
            "name" : "Hujan di Bulan Juni",
            "desc" : "Poetry Collection",
            "price" : 5.79,
            "image" : "https://res.cloudinary.com/amalia-kivansa/image/upload/v1644919146/online-shop/book-15_ait1uy.jpg"

        },

    ];

const productSlice = createSlice({
    name : "products",
    initialState : initialState,
    reducers : {},
})

export default productSlice.reducer

// const initialState = {
//     products : []
// };

// export const productsFetch = createAsyncThunk(
//     "products/productsFetch",
//     async({rejectWithValue}) => {
//         try{
//             const response = await axios.get("http://localhost:3000/products")
//             console.log(response);
//             return response?.data
//         } catch(error){
//             return rejectWithValue("an error occured")
//         }
//     }
// );

// const productSlice = createSlice({
//     name : "products",
//     initialState,
//     reducers : {},
// })

// export default productSlice.reducer