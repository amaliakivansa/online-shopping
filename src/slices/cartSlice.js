import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cartItems: [],
  cartTotalQuantity: 0,
  cartTotalAmount: 0,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart(state, action) {
      const itemIndex = state.cartItems.findIndex(
        (item) => item.id === action.payload.id
      );
      if (itemIndex >= 0) {
        state.cartItems[itemIndex].cartQuantity += 1;
      } else {
        const tempProduct = { ...action.payload, cartQuantity: 1 };
        state.cartItems.push(tempProduct);
      }
    },
    removeFromCart(state, action) {
      const cartItemsFilter = state.cartItems.filter(
        (item) => item.id !== action.payload.id
      );
      state.cartItems = cartItemsFilter;
    },
    decreaseItem(state, action) {
      const selectedItem = state.cartItems.findIndex(
        (item) => item.id === action.payload.id
      );

      if (state.cartItems[selectedItem].cartQuantity > 1) {
        state.cartItems[selectedItem].cartQuantity -= 1;
      } else if (state.cartItems[selectedItem].cartQuantity === 1) {
        const cartItemsFilter = state.cartItems.filter(
          (item) => item.id !== action.payload.id
        );
        state.cartItems = cartItemsFilter;
      }
    },
    clearCart(state, action){
      state.cartItems = []
    },
    getTotal(state, action){
      let {total, quantity} = state.cartItems.reduce((cartTotal, cartItem) => {
        const {price, cartQuantity} = cartItem;
        const itemTotal = price * cartQuantity;

        cartTotal.total += itemTotal;
        cartTotal.quantity +=  cartQuantity;

        return cartTotal
      }, {
        total : 0,
        quantity : 0
      })

      state.cartTotalAmount = total;
      state.cartTotalQuantity = quantity;
    }
  },
});

export const { addToCart, removeFromCart, decreaseItem, clearCart, getTotal } = cartSlice.actions;

export default cartSlice.reducer;
