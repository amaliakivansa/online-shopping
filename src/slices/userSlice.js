import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../helpers/axiosInstance";

export const login = createAsyncThunk(
  "auth/login",
  async ({ email, password }, thunkAPI) => {
    const bodyFormData = new FormData();
    bodyFormData.append("grant_type", "password");
    bodyFormData.append("client_id", "7b48f795-a4b1-4506-8f48-d955935e4002");
    bodyFormData.append("client_secret", "demo-secret");
    bodyFormData.append("username", email);
    bodyFormData.append("password", password);
    bodyFormData.append("scope", "*");

    try {
      const response = await axiosInstance({
        method: "post",
        url: "/api/oauth/token",
        data: bodyFormData,
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      console.log(response.data.data);
      let currTime = new Date();
      let timeExp = response.data.data.expires_in;
      let tokenExpired = currTime.getTime() / 1000 + timeExp;

      if (response.status === 200) {
        localStorage.setItem("token", response.data.data.access_token);
        console.log(response.data.data.access_token);
        localStorage.setItem("token-expired", tokenExpired);
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      console.log("Error", e.response.data);
      thunkAPI.rejectWithValue(e.response.data.data);
    }
  }
);

export const signUp = createAsyncThunk(
  "auth/register",
  async ({ fullname, email, password, password_confirmation }, thunkAPI) => {
    const bodyFormData = new FormData();
    bodyFormData.append("fullname", fullname);
    bodyFormData.append("email", email);
    bodyFormData.append("password", password);
    bodyFormData.append("password_confirmation", password_confirmation);
    try {
      const response = await axiosInstance(
        "https://accounts.tujjudemo.com/api/user",
        {
          url: "/api/user",
          method: "post",
          body: bodyFormData,
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      if (response.status === 200) {
        localStorage.setItem('token', response.token);
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      console.log("Error", e.response.data.data.errors.email);
      return thunkAPI.rejectWithValue(e.response.data.data);
    }
  }
);

export const getUser =  createAsyncThunk(
  "auth/getUser",
  async ( { token }, thunkAPI) => {
    try {
      const response = await axiosInstance({
        method: "get",
        url: "/api/me/user",
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization : token
        },
      });
      console.log(response.data);
      const data = response.data.data
      if (response.status === 200) {
        return { ...data };
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      console.log("Error", e.response.data.data);
      thunkAPI.rejectWithValue(e.response.data.data);
    }
  }
);

const initialState = {
  fullname : '',
  isFetching: false,
  isError: false,
  isSuccess: false,
  errorMessage: "",
};

const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    getUser : (state, action) => {
      state.fullname = action.payload;
    },
    reset : () => initialState,
  },
  extraReducers: {
    [login.fulfilled]: (state, { payload }) => {
      console.log("payload", payload);
      state.email = payload.email;
      state.isFetching = false;
      state.isSuccess = true;

      return state;
    },
    [login.rejected]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [login.pending]: (state) => {
      state.isFetching = true;
    },
    [signUp.fulfilled]: (state, { payload }) => {
      state.isFetching = false;
      state.isSuccess = true;
      state.email = payload.email;
    },
    [signUp.rejected]: (state, { payload }) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = payload.message;
    },
    [signUp.pending]: (state) => {
      state.isFetching = true;
    },
    [getUser.fulfilled] : (state, {payload}) => {
      state.isFetching = false;
      state.isSuccess = true;
      state.fullname = payload.fullname
    },
    [getUser.rejected] : (state) => {
      state.isFetching = false;
      state.isError = true;
    },
    [getUser.pending] : (state) => {
      state.isFetching = true;
    }
  },
});

export const { reset } = userSlice.actions;

export default userSlice.reducer;
