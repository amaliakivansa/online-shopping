import axios from "axios";

const token = localStorage.getItem("token");
const axiosInstance = axios.create({
  baseURL: "https://accounts.tujjudemo.com",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  },
});

const refreshToken = function refreshToken(request) {
  const tokenExpired = localStorage.getItem("token-expired");

  if (
    `${request.baseURL}oauth/login` === request.url ||
    request.url === "oauth/login" ||
    `${request.baseURL}oauth/revoke` === request.url ||
    request.url === "oauth/revoke" ||
    `${request.baseURL}oauth/token` === request.url ||
    request.url === "oauth/token"
  ) {
    return request;
  }

  // Refresh token
  if (tokenExpired && Date.now() > tokenExpired) {
    axiosInstance.post("/api/oauth/token", {
      body: JSON.stringify({
        grant_type: "refresh_token",
        client_id: "7b48f795-a4b1-4506-8f48-d955935e4002",
        client_secret: "demo-secret",
        scope: "*",
        refresh_token:
          "def50200acd16e34c0c2e2ffd4e83cdba761e08f19f0ba1b91d5c74ddf80cf831ac70e0f2de7237678b8baee0204c7e13a803b46a48935bdcdeed9c73d66f2a654a6feae47e8320cb5aa7fbf210971cb9b6fcf5a187b666ac67d7854cb559c53db2da24790c28e55fcad8054130b16fa193e9f2d998d9f69498b280955afc4de4a71c0aa417a6e9374376ba0ef0529f8523590b64131568aecf30714978b3d5e35220a87725851efd3704381fe736972781209c660ddaecf97fd420a855fe54f9d1810589671bb8bfeab8bc6a1693a49f001aa3406af7df9422b372c84241cefcaf636ca09ef1535598a47fa72892390fa3d1d73718ac6b3fa0843bf2cc018235c1488783baf876ccbd1445462b50e8e275903261d69b811f64715e71cd05f831b9a3f920b86779517fc487be7784dd3badf5fe0051d6d6936c37feb02cc617a2b97a253ea46955b9af903b09feb0062769038fc389d5c6b7807a40d1e3387c03529632f181b8ce6cb5d4feacf4e3a0baa7f40f4cd3c3c544edcdae98170a4874fa018953cdce1e1",
      }),
    });
  }

  return request;
};

axiosInstance.interceptors.request.use(refreshToken);

export default axiosInstance;
