import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Home from "./components/Home";
import Navbar from "./components/Navbar";
import Cart from "./components/Cart";
import NotFound from "./components/NotFound";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import { PrivateRoute } from "./helpers/PrivateRoute";
import Agent from "./components/Agent";
import Client from "./components/Client";
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar/>
        <Switch>
          <Route exact component={Login} path="/login" />
          <Route exact component={SignUp} path="/signup" />
          <PrivateRoute path="/" component={Home} exact />
          <PrivateRoute path="/cart" component={Cart} />
          <PrivateRoute path="/client" component={Client} />
          <PrivateRoute path="/agent" component={Agent} />
          <PrivateRoute path="/not-found" component={NotFound} />
          <Redirect to="/not-found" />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
